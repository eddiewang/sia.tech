# sia.tech website

## Installation

Run `yarn`

## Development

Run `yarn dev` to start local development server

## New release process

1. Generate docs and commit them to this repo
1. Update binaries version number in `app/pages/GetStarted.tsx` (use find & replace)
1. In `server/index.js` update `/docs` endpoint to new version number and add the new endpoint for new version number
1. On server, copy over the binaries to `~/sia.tech/public/releases`
1. On server, rebuild and restart website
   1. pull latest master with `git pull`
   1. run `yarn` to install dependencies
   1. restart pm2 process with `pm2 restart sia`
1. verify the deployment
   1. check that the docs are pointing to up to date version
   1. check that all the binaries are downloading
