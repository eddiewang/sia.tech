import * as React from "react";
import * as styles from "./Releases.scss";
import classNames from "classnames";
import { inject, observer } from "mobx-react";

import LayoutContainer from "components/LayoutContainer";
import TypeHeading from "components/TypeHeading";
import Section from "components/Section";
import Helmet from "react-helmet";
import MediaCard from "components/MediaCard";
import Text from "components/Text";

@inject("main")
@observer
class SkyDB2020 extends React.Component {
  public render() {
    return (
      <div>
        <Helmet title="Newsroom" />
        <Section>
          <LayoutContainer classes={styles.Vision}>
            <div className={styles.VisionBody}>
              <Text.Paragraph type="visionHeading">
                Skynet Announces SkyDB, Unlocking a Fully Decentralized Internet
              </Text.Paragraph>

              <Text.Paragraph>
                <i>SkyDB enables decentralized social media to take on &#96; The Social Dilemma&#96;</i>
              </Text.Paragraph>

              <Text.Paragraph bold>Boston, MA – October 15th, 2020</Text.Paragraph>

              <Text.Paragraph>
                Skynet Labs, the company building the{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia
                </a>{" "}
                decentralized cloud storage platform and the{" "}
                <a href="https://siasky.net" target="_blank">
                  Skynet
                </a>{" "}
                application hosting platform, today announced the launch of SkyDB, a framework that allows developers to
                build feature-complete applications that compete with the likes of Youtube, Twitter, Instagram, and
                Tiktok.
              </Text.Paragraph>

              <Text.Paragraph>
                The fabric of our society and the way we interact with each other has been heavily influenced and
                altered by the rise of social media platforms. Biased algorithms amplify stereotypes, constant streams
                of content compete for our attention, and manipulated narratives wedge digital divides across political
                ideologies and cultural identities. As pertinently pointed out by the popular Social Dilemma
                documentary, “We have moved from a tools-based technology environment to an addiction and manipulation
                based technology environment"
              </Text.Paragraph>

              <Text.Paragraph>
                SkyDB enables developers to realize the full potential of a{" "}
                <a href="https://blog.sia.tech/the-war-for-a-free-internet-c0a7fcc00c46" target="_blank">
                  Free Internet
                </a>
                . Building these Web3 applications not only has benefits of privacy and control over one’s data but it
                ushers innovation currently impossible in the siloed centralized web of today. SkyDB is a framework that
                allows users to create decentralized accounts and store mutable data in those accounts which can be
                accessed globally from any device. The user does not need to sync any blockchains or run any special
                software — a normal web browser is sufficient. For more details on the inner workings of SkyDB and the
                potential it unlocks, check out our latest{" "}
                <a
                  href="https://blog.sia.tech/skydb-a-mutable-database-for-the-decentralized-web-7170beeaa985"
                  target="_blank"
                >
                  blog post
                </a>
                .
              </Text.Paragraph>

              <Text.Paragraph>
                “Today is a turning point in history where the decentralized web starts to eat the centralized web,”
                said David Vorick, Skynet Labs CEO and lead developer. “Skynet gives developers more tools to create
                better experiences for users. Skynet in many senses is the first true cloud, where all data is available
                everywhere, regardless of the original application.”
              </Text.Paragraph>

              <Text.Paragraph>
                Skynet was{" "}
                <a href="https://blog.sia.tech/skynet-bdf0209d6d34" target="_blank">
                  launched
                </a>{" "}
                back in February 2020 and since then has seen accelerated growth in developer interest and usage. Users
                around the world have uploaded more than 2.5 million files amounting to 15+ TeraBytes of data using
                Skynet. In October, Skynet crossed a record-high of 150,000 downloads in a single day.
              </Text.Paragraph>

              <Text.Paragraph>
                A thriving community of developers has built over 150 applications in the span of a few months including
                a Video Streaming app{" "}
                <a href="https://skylive.coolhd.hu/" target="_blank">
                  SkyLive
                </a>
                , Blogging apps like{" "}
                <a href="https://siasky.net/hns/wakio" target="_blank">
                  Wakio
                </a>{" "}
                and{" "}
                <a href="https://siasky.net/DADAgjHeqGnQ1hIEET54YyIZhBN_oz51HMogZsLiS60KTA/" target="_blank">
                  Skyblog Builder
                </a>
                , Video & Image gallery app{" "}
                <a href="https://skygallery.xyz/" target="_blank">
                  SkyGallery
                </a>
                , a Decentralized AppStore{" "}
                <a href="https://github.com/redsolver/skydroid" target="_blank">
                  Skydroid
                </a>
                . The full list of Skynet Apps can be explored at the{" "}
                <a href="https://siasky.net/hns/skyapps/" target="_blank">
                  Skynet AppStore
                </a>
                , and an English language{" "}
                <a
                  href="https://siasky.net/_BmN2WhKvKAm0CzsfmHyiP81yUnYyQxh5To8JkVbj1RUxg/www/index.html"
                  target="_blank"
                >
                  Wikipedia clone
                </a>
                .
              </Text.Paragraph>

              <Text.Paragraph>
                “Next-generation applications are already being built on Skynet with more developers joining the
                community every day,” said Manasi Vora, Skynet Labs’ VP Strategy & Operations. “Skynet shines in how its
                easy to use not just for developers but also for the end-users, a rarity in the blockchain space.”
              </Text.Paragraph>

              <Text.Paragraph>
                By building on the{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia network
                </a>
                , Skynet delivers a 10x reduction in storage costs and a 100x reduction in bandwidth costs when compared
                to centralized providers, without sacrificing performance or reliability. Skynet achieves 1 gigabit per
                second in download and uploads speeds, with more improvements coming in future releases.
              </Text.Paragraph>

              <Text.Paragraph>
                Interested in Skynet? Learn more at{" "}
                <a href="https://siasky.net" target="_blank">
                  Siasky.net
                </a>{" "}
                join us on{" "}
                <a href="https://discord.gg/sia" target="_blank">
                  Discord
                </a>
                {""}, and email us at hello@sia.tech.
              </Text.Paragraph>

              <TypeHeading level={6}>About Skynet Labs</TypeHeading>
              <Text.Paragraph>
                Skynet Labs, previously known as{" "}
                <a href="https://nebulous.tech" target="_blank">
                  Nebulous
                </a>
                , builds uncompromising software infrastructure for the decentralized internet. This includes{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia
                </a>
                , the leading decentralized cloud storage platform, and{" "}
                <a href="https://siasky.net" target="_blank">
                  Skynet
                </a>
                , a content and application hosting platform.
              </Text.Paragraph>

              <Text.Paragraph>
                Skynet Labs defines uncompromising infrastructure as scalable, trustless, secure, and – most important –
                fully decentralized. In a blockchain industry filled with hype but lacking substance, Skynet Labs stands
                out as one of the few deeply technical teams that consistently deliver real products with significant
                potential.
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous, Inc. was founded in 2014 and is headquartered in Boston. It is funded by Paradigm, Bain
                Capital Ventures, A.Capital, Bessemer Venture Partners, Dragonfly Capital Partners, First Star Ventures,
                and other notable investors.
              </Text.Paragraph>
            </div>
          </LayoutContainer>
        </Section>
      </div>
    );
  }
}

export default SkyDB2020;
