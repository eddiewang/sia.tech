import * as React from "react";
import * as styles from "./Releases.scss";
import classNames from "classnames";
import { inject, observer } from "mobx-react";

import LayoutContainer from "components/LayoutContainer";
import TypeHeading from "components/TypeHeading";
import Section from "components/Section";
import Helmet from "react-helmet";
import MediaCard from "components/MediaCard";
import Text from "components/Text";

@inject("main")
@observer
class Funding2019 extends React.Component {
  public render() {
    return (
      <div>
        <Helmet title="Newsroom" />
        <Section>
          <LayoutContainer classes={styles.Vision}>
            <div className={styles.VisionBody}>
              <Text.Paragraph type="visionHeading">
                Sia Announces Skynet, the Storage Foundation for a Free Internet
              </Text.Paragraph>

              <Text.Paragraph>
                <i>Skynet is available with Sia version 1.4.3 and ready to use today!</i>
              </Text.Paragraph>

              <Text.Paragraph bold>Boston, MA – February 18th, 2020</Text.Paragraph>

              <Text.Paragraph>
                Nebulous, the company building the Sia decentralized cloud storage network, today{" "}
                <a href="https://blog.sia.tech/skynet-bdf0209d6d34" target="_blank">
                  announced
                </a>{" "}
                the launch of{" "}
                <a href="https://siasky.net" target="_blank">
                  Skynet
                </a>
                , a decentralized CDN and file sharing platform for application developers. Skynet enables high speed,
                low cost, and superior infrastructure to serve as the storage foundation for a free Internet.
              </Text.Paragraph>

              <Text.Paragraph>
                Today’s decentralized applications (dapps) largely rely on centralized data storage providers like
                Amazon, due to the lack of reliable, fast, and production-ready decentralized alternatives. This makes
                it difficult for developers to build truly decentralized applications and prevents the dapp ecosystem
                from realizing the real potential of a{" "}
                <a href="https://blog.sia.tech/the-war-for-a-free-internet-c0a7fcc00c46" target="_blank">
                  Free Internet
                </a>
                .
              </Text.Paragraph>

              <Text.Paragraph>
                Skynet provides an easy-to-use data storage and publishing mechanism on which developers can build
                decentralized applications. With a simple API and SDKs for popular programming languages, Skynet
                empowers developers to easily integrate decentralized storage into their applications. Importantly,
                end-users can directly access files on Skynet without needing to run full nodes or deal with
                cryptocurrencies.
              </Text.Paragraph>

              <Text.Paragraph>
                By building on the{" "}
                <a href="https://sia.tech" target="_blank">
                  Sia Network
                </a>
                , Skynet delivers a 10x reduction in storage costs and a 100x reduction in bandwidth costs when compared
                to centralized providers, without sacrificing performance or reliability. Amazingly, Skynet achieves 1
                gigabit per second in download and uploads speeds, with more improvements coming in future releases.
              </Text.Paragraph>

              <Text.Paragraph>
                “After seven years of nonstop work, applications built on decentralized storage are finally viable. I
                can say without a doubt that Skynet is the most powerful tech we've ever built” said David Vorick,
                Nebulous CEO and Sia Lead Developer. “Skynet makes it possible for content and applications to be
                deployed to a decentralized network just in seconds and be immediately available to everyone across the
                world.”
              </Text.Paragraph>

              <Text.Paragraph>
                Our vision for Skynet involves supporting decentralized content publishing. Whether through news
                articles, blog posts, music, or video, Nebulous envisions Skynet as a decentralized and
                censorship-resistant foundation for content creators to deliver media their audience cares about. No
                more de-platforming, no more exploitation.
              </Text.Paragraph>

              <Text.Paragraph>
                “One place where Skynet shines is speed. Page loading is almost instant. Deployment to Skynet is also
                almost instant. We’ve demonstrated that decentralized storage can be as fast as its centralized
                competitors” said Manasi Vora, Nebulous’ Head of Product Strategy. “With storage and bandwidth prices so
                low, we expect Skynet to enable previously unimaginable applications and spark a renaissance in
                media-heavy applications.”
              </Text.Paragraph>

              <Text.Paragraph>
                Since Sia’s launch in 2015, users have stored over 4 PB of data across over 1 million storage smart
                contracts. Currently, the Sia network has 2.2 PB of available storage capacity and Sia software has been
                downloaded over 1 million times. Sia has a thriving community of third-party developers who have
                launched file sharing websites like{" "}
                <a href="https://pixeldrain.com" target="_blank">
                  PixelDrain
                </a>{" "}
                and{" "}
                <a href="https://storewise.tech/" target="_blank">
                  Storewise
                </a>
                , cloud offerings like{" "}
                <a href="https://filebase.com" target="_blank">
                  Filebase
                </a>{" "}
                and{" "}
                <a href="https://arzen.tech/" target="_blank">
                  Arzen
                </a>
                ,and companion apps like{" "}
                <a href="https://keops.cc/decentralizer" target="_blank">
                  Decentralizer
                </a>{" "}
                and{" "}
                <a href="https://siacentral.com" target="_blank">
                  SiaCentral
                </a>
                .
              </Text.Paragraph>

              <Text.Paragraph>
                Interested in Skynet? Learn more at{" "}
                <a href="https://siasky.net" target="_blank">
                  Siasky.net
                </a>{" "}
                join us on{" "}
                <a href="https://discord.gg/sia" target="_blank">
                  Discord
                </a>
                {""}, and email us at hello@sia.tech.
              </Text.Paragraph>

              <Text.Paragraph>
                Want to help us re-decentralize the Internet? Nebulous is hiring for positions in engineering and
                operations. Learn more about our projects and{" "}
                <a href="https://jobs.lever.co/nebulous" target="_blank">
                  apply today
                </a>
                !
              </Text.Paragraph>

              <TypeHeading level={6}>About Nebulous</TypeHeading>
              <Text.Paragraph>
                <a href="https://nebulous.tech" target="_blank">
                  Nebulous
                </a>{" "}
                builds uncompromising blockchain hardware and software infrastructure for the decentralized internet.
                This includes Sia, the leading decentralized cloud storage platform, and{" "}
                <a href="https://obelisk.tech" target="_blank">
                  Obelisk
                </a>
                , a producer of blockchain-related hardware.
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous defines uncompromising infrastructure as scalable, trustless, secure, and – most important –
                fully decentralized. In a blockchain industry filled with hype but lacking substance, Nebulous stands
                out as one of the few deeply technical teams that consistently delivers real products with significant
                potential.
              </Text.Paragraph>

              <Text.Paragraph>
                Nebulous, Inc. was founded in 2014 and is headquartered in Boston. Nebulous is funded by Bain Capital
                Ventures, A.Capital, Raptor Group, First Star Ventures, and other notable investors.
              </Text.Paragraph>
            </div>
          </LayoutContainer>
        </Section>
      </div>
    );
  }
}

export default Funding2019;
