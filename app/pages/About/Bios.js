import David from "svg/avatar-david.svg";
import PJ from "svg/avatar-pj.svg";
import Luke from "svg/avatar-luke.svg";
import Chris from "svg/avatar-chris.svg";
import Steve from "svg/avatar-steve.svg";
import Matt from "svg/avatar-matt.svg";
import Manasi from "svg/avatar-manasi.svg";
import MarcinS from "svg/avatar-marcins.svg";
import Karol from "svg/avatar-karol.svg";
import Ivaylo from "svg/avatar-ivaylo.svg";
import Filip from "svg/avatar-filip.svg";

export default [
  {
    name: "David Vorick",
    svg: David,
    role: "CEO, Cofounder, Lead Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/DavidVorick",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/DavidVorick",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/davidvorick",
      },
    ],
    content:
      "I like knowing that our technology is out there improving lives and changing the way people think about the world. I hope that what we build is inspirational to others; the desire to inspire is, in itself, an inspiration for me.",
  },
  {
    name: "Luke Champine",
    svg: Luke,
    role: "Cofounder, Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/lukechampine",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/lukechampine",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/lukechampine",
      },
    ],
    content:
      "Sia captures that rebellious hacker spirit that spurns the current paradigm and, instead, builds something novel, powerful, esoteric. Code is eating the world; blockchains are just a continuation of that trend, extending it into the world of finance.",
  },
  {
    name: "Chris Schinnerl",
    svg: Chris,
    role: "Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/ChrisSchinnerl",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/ChrisSchinnerl",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/ChrisSchinnerl",
      },
    ],
    content:
      "In a world of distributed storage there will be no downtime, no companies spying on your data, and no massive cloud leaks. Storage will be faster, safer, and cheaper.",
  },
  {
    name: "Steve Funk",
    svg: Steve,
    role: "Head of Support",
    social: [
      {
        title: "LinkedIn",
        url: "https://www.linkedin.com/in/stevengfunk",
      },
    ],
    content:
      "Sia has a team that truly cares about the experience, and a passionate community that inspires us to do our best.",
  },
  {
    name: "Matt Sevey",
    svg: Matt,
    role: "Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/MSevey",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/MSevey",
      },
      {
        title: "LinkedIn",
        url: "https://linkedin.com/in/sevey",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/MJSevey",
      },
    ],
    content:
      "Sia is leading the way and defining what decentralized data storage can be. It enables users to take control over how their data is stored and have the confidence it is safe and secure.",
  },
  {
    name: "Manasi Vora",
    svg: Manasi,
    role: "Head of Product Strategy",
    social: [
      {
        title: "LinkedIn",
        url: "http://linkedin.com/in/manasi-vora-cfa-bb9a1715",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/manasilvora",
      },
    ],
    content:
      "Sia is building the perfect marketplace for data storage with focus on privacy, cost and reliability. With a strong technical foundation, Sia is poised to be the storage layer of the decentralized internet.",
  },
  {
    name: "PJ Brone",
    content:
      "Through elegant design without compromise nor shortcuts, Sia is on its way to become the backbone of the decentralized web. I’m excited to contribute to tech that gives ownership of data back to users.",
    svg: PJ,
    role: "Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/peterjanbrone",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/pjbrone",
      },
      {
        title: "LinkedIn",
        url: "https://www.linkedin.com/in/peterjanbrone",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/peterjanbrone",
      },
    ],
  },
  {
    name: "Marcin Swieczkowski",
    content:
      "Sia is a truly exciting and innovative technology that will reshape the way we think about our data, threatening the existing centralized interests and giving power back to the people.",
    svg: MarcinS,
    role: "Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/m-cat",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/m-cat",
      },
    ],
  },
  {
    name: "Karol Wypchlo",
    content:
      "Do you know where your cloud data is stored? Do you know who has access to it? Do you know who manages it? Is the data still yours then? Sia can answer all these questions and doubts, giving you full governance of your cloud data in a secure and transparent way.",
    svg: Karol,
    role: "Full Stack Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/kwypchlo",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/kwypchlo",
      },
      {
        title: "LinkedIn",
        url: "https://www.linkedin.com/in/karolwypchlo/",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/kwypchlo",
      },
    ],
  },
  {
    name: "Ivaylo Novakov",
    content:
      "Scale is hard - it involves expensive data centres and complicated failure modes. But it doesn’t have to be! There are so many resources already out there, sitting idly and waiting to be utilised.  All we need is a way to offer them to the people who need them. And Sia does just that.",
    svg: Ivaylo,
    role: "Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/ro-tex",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/ro-tex",
      },
      {
        title: "LinkedIn",
        url: "https://www.linkedin.com/in/inovakov/",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/inovakov",
      },
    ],
  },
  {
    name: "Filip Rysavy",
    content:
      "We are building Sia to offer you what are you looking for regarding your data storage needs. It is simple, fast, reliable, distributed, open source solution backed by a friendly team of professionals and community.",
    svg: Filip,
    role: "Software Engineer, Testing",
    social: [
      {
        title: "LinkedIn",
        url: "https://www.linkedin.com/in/filiprysavy/",
      },
    ],
  },
];
