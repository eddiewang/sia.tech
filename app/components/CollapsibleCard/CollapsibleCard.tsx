import React from "react";
import Text from "components/Text";
import Icon from "components/Icon";

import classNames from "classnames";

class CollapsibleCard extends React.Component<Props, {}> {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };

    this.toggleCollapse = this.toggleCollapse.bind(this);
  }

  toggleCollapse(e) {
    e.preventDefault();
    this.setState({ open: !this.state.open });
  }

  render() {
    const { children, buttonText } = this.props;

    return (
      <React.Fragment>
        <button type="button" onClick={(e) => this.toggleCollapse(e)}>
          <strong>{buttonText}</strong>
        </button>
        {this.state.open ? <React.Fragment>{children}</React.Fragment> : null}
      </React.Fragment>
    );
  }
}

export default CollapsibleCard;
